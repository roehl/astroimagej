# AstroImageJ in der Amateurastronomie




### Tipp:

    git clone git clone https://roehl@bitbucket.org/roehl/astroimagej.git astroimagej
### LINKS

* Buchhomepage http://www.softure.de/
* Homepage des **CAS-Observatory** http://astronomy.fsg-preetz.de Dies ist die private Sternwarte des Autors.

In der Linkliste des **CAS-Observatory** für das Astronomie Software Praktikum sind Anleitungen & Tutorials rund um AstroImageJ aufgeführt.

* http://astronomy.fsg-preetz.de/doku.php?id=download_links  **AstroImageJ-Tabelle** 

